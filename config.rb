###
# Compass
###

# Change Compass configuration
# compass_config do |config|
#   config.output_style = :compact
# end

###
# Page options, layouts, aliases and proxies
###

# Per-page layout changes:
#
# With no layout
# page "/path/to/file.html", :layout => false
#
# With alternative layout
# page "/path/to/file.html", :layout => :otherlayout
#
# A path which all have the same layout
# with_layout :admin do
#   page "/admin/*"
# end

# Proxy pages (http://middlemanapp.com/basics/dynamic-pages/)
# proxy "/this-page-has-no-template.html", "/template-file.html", :locals => {
#  :which_fake_page => "Rendering a fake page with a local variable" }

###
# Helpers
###

# Automatic image dimensions on image_tag helper
# activate :automatic_image_sizes

# Reload the browser automatically whenever files change
# configure :development do
#   activate :livereload
# end

# Methods defined in the helpers block are available in templates
helpers do
  def image_list(names=[], dir='', def_title='', def_alt = '')
    html = ''
    names.each do |name|
      item = [*name]
      name = item[0]
      title = item[1] || def_title || ''
      alt = item[2] || def_alt || ''  

      html += link_to image_tag("galeria/#{dir}/thumbs/#{name}", class: 'img-responsive img-thumbnail', title: title, alt: alt, data: {toggle: 'tooltip'}),
              image_path("galeria/#{dir}/#{name}"),
              class: 'col-lg-3 col-md-4 col-xs-6',
              data: {toggle: 'lightbox', gallery: 'multiimages', title: title, footer: title}
    end
    html
  end
end

set :css_dir, 'stylesheets'

set :js_dir, 'javascripts'

set :images_dir, 'images'

activate :bh

# Build-specific configuration
configure :build do

  # For example, change the Compass output style for deployment
  # activate :minify_css

  # Minify Javascript on build
  # activate :minify_javascript

  # Enable cache buster
  # activate :asset_hash

  # Use relative URLs
  # activate :relative_assets

  # Or use a different image path
  # set :http_prefix, "/Content/images/"
end
