//= require 'jquery'
//= require 'bootstrap'
//= require 'ekko-lightbox'
//= require_tree .

$(function(){
    $(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
        event.preventDefault();
        $(this).ekkoLightbox();
    });
});

$(function () {
  $('[data-toggle="tooltip"]').tooltip();
})